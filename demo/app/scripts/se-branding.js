'use strict';
angular.module('se-branding').controller('landingPage', ['$scope', '$mdToast', 'sdkTemplateServices', '$http', '$window', 'gettextCatalog', 'toasterService', '$log', '$sce', 'authorizationService', '$location', '$routeParams', '$q', '$timeout', '$filter','$rootScope',

    function($scope, $mdToast, sdkTemplateServices, $http, $window, gettextCatalog, toasterService, $log, $sce, authorizationService, $location, $routeParams, $q, $timeout, $filter,$rootScope) {
        authorizationService.setUserHavePermissionCallback(function(permissions) {
            var guestUser = false;
            if (angular.isArray(permissions)) {
                angular.forEach(permissions, function(permission) {
                    if (permission === 'GUEST') guestUser = true;
                });
            } else if (angular.isString(permissions)) {
                if (permissions === 'GUEST') guestUser = true;
            }
            return guestUser;
        });
        $scope.seHeaderComponent = "seHeaderComponent1-1-0";
        $scope.mainfooterlinks = [];
        sdkTemplateServices.fetch('data/se-main-footer-links.json').then(function(data) {
            $scope.mainfooterlinks = data;
        });
        $scope.footerSocialLinks = [];
        sdkTemplateServices.fetch('data/se-social-contents.json').then(function(data) {
            $scope.footerSocialLinks = data;
        });
        //Listen Language Change
        $scope.$on('languageChanged', function(event, data) {
          console.log(data);
            $log.info("Callback event triggered post language changed");
        });
        //Alternate Footer configuration settings
        sdkTemplateServices.fetch('data/se-footer-alt-config.json').then(function(data) {
            $scope.footerconfig = data;
        });

        sdkTemplateServices.fetch('data/se-countries-languages.json').then(function(data) {
            $scope.locales = data;
        });
        // Custom Header and Navigation Data
        //code includes
        $scope.scriptInclude = '<script src="bower_components/se-header-component/se-header-component-all.js" type="text/javascript"></script>';
        $scope.cssInclude = '<link href="bower_components/se-header-component/se-header-component-all.css" rel="stylesheet"/>';
        //naviagation
        $scope.$on('$routeChangeSuccess', function(event, current, previous) {
            if (angular.isDefined(current.$$route) && current.$$route.originalPath.indexOf('/homepage') === -1) {
                $scope.component = undefined;
                return;
            }
            $scope.component = $scope.totalItems[$scope.findIndex($scope.totalItems, current.pathParams.component)];
            if (!current.pathParams.component) {
                $scope.component = $scope.totalItems[0];
            }
        });
        $scope.findIndex = function(arr, val) {
            var index;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].modelValue === val) {
                    index = i;
                    break;
                }
            }
            return index;
        };
        $scope.nav = function(page) {
            $scope.component = $scope.totalItems[$scope.findIndex($scope.totalItems, page)];
        };
        $scope.$watch('component', function() {
            if (!$scope.component) {
                return;
            }
            $scope.componentPath = 'views/component-docs/' + $scope.component.modelValue + '.html';
            $location.path('/homepage/' + $scope.component.modelValue);
        });

        $scope.topics = [{
            "title": "Getting Started",
            "name": "Getting Started",
            "modelValue": "GettingStarted"
        }, {
            "title": "Files to download",
            "name": "Files to download",
            "modelValue": "Filestodownload"
        }, {
            "title": "Installation",
            "name": "Installation",
            "modelValue": "Installation"
        }, {
            "title": "Migration",
            "name": "Migration",
            "modelValue": "migration"
        }];
        $scope.directives = [{
            "title": "seCustomHeader",
            "name": "seCustomHeader",
            "modelValue": "seCustomHeader"
        },{
          "title": "seCustomHeaderExample",
          "name": "seCustomHeaderExample",
          "modelValue": "seCustomHeaderExample"
        }];
        $scope.totalItems = $scope.topics.concat($scope.directives);
      $scope.confirm = function(hideModal) {
        var deferred = $q.defer();
        if (hideModal) {
          $('#se-country-language-modal').modal('hide');
        }
        $('#myModal').modal('show');
        $scope.confirmVal = function(val) {
          if (val) {
            deferred.resolve(val);
          } else {
            deferred.reject(val);
          }
          $('#myModal').modal('hide');
        };
        return deferred.promise;
      }
      //callbacks for header directive function starts
        /*$scope.onbeforeLogout = function(){
          var deferred = $q.defer();
          $scope.modalTitle = "Logout Intercept Event Demo";
          $scope.modalContent = "Want to really logout?";
          $scope.modalOkay = "Yes";
          $scope.modalCancel = "No";
          $scope.confirm().then(function(val) {
            deferred.resolve(val);
          }, function(err) {
            deferred.reject(err);
          });
          return deferred.promise;
        };*/
      $scope.onbeforeLogout = function() {
        alert('logout callback called');
        var deferred = $q.defer();
        var confirmation = true;
        deferred.resolve(confirmation);
        return deferred.promise;
      };
        sdkTemplateServices.fetch('data/se-countries-languages.json').then(function(data) {
        $scope.locales = data;
        });
        //callbacks for header directive function ends
         $scope.myName = "seHeaderComponent";
    }
]);
