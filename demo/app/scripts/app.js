'use strict';
/**
 * @ngdoc brandingModule
 * @name se-branding
 * @description
 * # se-branding
 *
 * Main module of the application.
 */
angular.module('se-branding', ['seWebUI', 'gettext', 'ui.bootstrap', 'seHeaderComponent', 'hljs']).config(['$routeProvider', "$sceDelegateProvider", function($routeProvider, $sceDelegateProvider) {
    $routeProvider.when('/homepage', {
        templateUrl: 'views/se-landing.html',
        resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService', function($rootScope, seLandingMetaInfo, authenticationService) {
                //authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function(data) {
                    $rootScope.metatags = data.homepage;
                });
            }]
        }
    }).when('/homepage/:component', {
        templateUrl: 'views/se-landing.html',
        resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService', function($rootScope, seLandingMetaInfo, authenticationService) {
                //authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function(data) {
                    $rootScope.metatags = data.homepage;
                });
            }]
        }
    }).when('/searchresults', {
        templateUrl: 'views/search.html',
        resolve: {
            app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService', function($rootScope, seLandingMetaInfo, authenticationService) {
                //authenticationService.checkLogin();
                seLandingMetaInfo.getMetaInfo().then(function(data) {
                    $rootScope.metatags = data.homepage;
                });
            }]
        }
    }).when('/header', {
      templateUrl: 'views/se-header.html',
      resolve: {
        app: ['$rootScope', 'seLandingMetaInfo', 'authenticationService', function($rootScope, seLandingMetaInfo, authenticationService) {
          //authenticationService.checkLogin();
          seLandingMetaInfo.getMetaInfo().then(function(data) {
            $rootScope.metatags = data.homepage;
          });
        }]
      }
    }).otherwise({
        redirectTo: '/homepage'
    });
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'http://10.194.156.27:9019/**'
    ]);
}]).constant('loginConstants', (function() {
    return {
        LOGIN_URL: '/authorize',
        LOGOUT_URL: '/oauth/disconnect',
        USER_SERVICE_URL: '/api/v1/user'
    }
})()).run(['$rootScope', '$location', '$http', '$q', 'authenticationService', 'seClient', 'implicitGrantLoginService', 'authorizationCodeGrantService', 'loginConstants', function($rootScope, $location, $http, $q, authenticationService, seClient, implicitGrantLoginService, authorizationCodeGrantService, loginConstants) {
    authenticationService.setAuthenticationUrl('https://login-ppr.schneider-electric.com');
    authenticationService.setClientId(seClient.CLIENT_ID);
    authenticationService.setGmrCode(seClient.GMR_CODE);
    authenticationService.setLoginService(implicitGrantLoginService);
    authenticationService.setLoginConstants(loginConstants);
    //authenticationService.setLoginService(authorizationCodeGrantService);
    $rootScope.$on('$routeChangeStart', function(event, route) {
        authenticationService.checkLogin();
    });
}]).config(function(hljsServiceProvider) {
    hljsServiceProvider.setOptions({
        // replace tab with 4 spaces
        tabReplace: '    '
    });
});
