module.exports = function(config) {
    config.set({
        basePath: '../',
        frameworks: ['jasmine'],
        files: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-gettext/dist/angular-gettext.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            'bower_components/angular-sanitize/angular-sanitize.js',
            'bower_components/ng-scrollbar/dist/ng-scrollbar.js',
            'bower_components/angular-animate/angular-animate.js',
            'bower_components/angular-aria/angular-aria.js',
            'bower_components/angular-cookies/angular-cookies.js',
            'bower_components/angular-messages/angular-messages.js',
            'bower_components/angular-resource/angular-resource.js',
            'bower_components/angular-route/angular-route.js',
            'bower_components/angular-material/angular-material.js',
            'bower_components/angular-gettext/dist/angular-gettext.js',
            'bower_components/angular-touch/angular-touch.js',
            'bower_components/webui/dist/web-ui-component-all-min.js',
            'scripts/main.js',
            'scripts/directives/*.js',
            'scripts/controllers/*.js',
            'scripts/**/*.js',
            'tests/*/*.js',
            'tests/**/*.js',
            'templates/**/*.html',
            // read json
            {
                pattern: 'tests/mock/**/*.json',
                included: false
            },
        ],
        preprocessors: {
            'templates/*.html': 'ng-html2js',
            'scripts/**/*.js': ['coverage']
        },
        ngHtml2JsPreprocessor: {
            // If your build process changes the path to your templates,
            // use stripPrefix and prependPrefix to adjust it.
            stripPrefix: "templates/",
            //prependPrefix: "app/templates/",
            // the name of the Angular module to create
            moduleName: "seHeaderComponent"
        },
        reporters: ['progress', 'coverage', 'html'],
        htmlReporter: {
            outputDir: 'karma_html', // where to put the reports  
            templatePath: null, // set if you moved jasmine_template.html 
            focusOnFailures: true, // reports show failures on start 
            namedFiles: false, // name files instead of creating sub-directories 
            pageTitle: null, // page title for reports; browser info by default 
            urlFriendlyName: false, // simply replaces spaces with _ for files/dirs 
            reportName: 'report-summary-filename', // report summary filename; browser info by default          
            // experimental 
            preserveDescribeNesting: false, // folded suites stay folded  
            foldAll: false, // reports start folded (only with preserveDescribeNesting) 
        },
        colors: true,
        autoWatch: false,
        browsers: ['PhantomJS2'],
        singleRun: true,
        plugins: ['karma-phantomjs2-launcher', 'karma-jasmine', 'karma-ng-html2js-preprocessor', 'karma-html-reporter', 'karma-coverage']
    });
};