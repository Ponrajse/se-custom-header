'use strict';
/* jshint -W097 */
angular.module('seHeaderComponent').controller('seCustomHeaderController', ['$scope', '$timeout' ,'$rootScope', 'gettextCatalog','$q', function($scope, $timeout, $rootScope, gettextCatalog, $q) {
    $scope.userLoggedIn = false;
    $scope.lang = false;
     //default language function
        $scope.defaultLanguage = function() {
            angular.forEach($scope.locale, function (obj) {
                angular.forEach(obj.Countries, function (countries) {
                    if (countries.default === true && $scope.lang === false) {
                        $scope.selectedCountryLangauge = countries.name;
                        gettextCatalog.setCurrentLanguage(countries.value);
                    } else if($scope.lang === true){
                        $scope.selectedCountryLangauge = gettextCatalog.getCurrentLanguage();
                    }
                });
            });
        };
    $timeout(function () {
        $scope.defaultLanguage();
        $scope.$apply();
    },100);

    //setting the user profile after logged in to the application
    $rootScope.$on('event:loginConfirmed', function(event, data) {
        $scope.loginUser = data.loginUser;
        if($scope.loginUser){
            $scope.userName = $scope.loginUser.firstName.charAt(0)+$scope.loginUser.lastName.charAt(0);
            $scope.userFullName = $scope.loginUser.firstName+'  '+$scope.loginUser.lastName;
            $scope.userLoggedIn = true;
        } else{
            $scope.userLoggedIn = false;
        }
    });

    //language changed based on the user selection
    $scope.CountryLanguageOptionChanged = function(countryLanguageObj,regionName) {
        if (angular.isUndefined(countryLanguageObj.value)) {
            return;
        }
        $scope.selectedCountryLangauge = countryLanguageObj.name;
        gettextCatalog.setCurrentLanguage(countryLanguageObj.value);
        $scope.lang = true;
        $rootScope.$broadcast('languageChanged', {'countryLanguage' : countryLanguageObj,'region' : regionName});
    };

    //watching and setting default language
    $scope.$watch('locales', function (newValue) {
        angular.forEach($scope.locales, function (obj) {
            angular.forEach(obj.Countries, function (countries) {
                if (countries.default === true && $scope.lang === false) {
                    $scope.selectedCountryLangauge = countries.name;
                    $scope.lang = true;
                    gettextCatalog.setCurrentLanguage(countries.value);
                }
            });
        });
    });

    //hamburger sidedrawer open
    var trigger = angular.element('.hamburgerIcon'),
        overlay = angular.element('.overlay'),
        isClosed = false;

    trigger.click(function () {
        hamburger_cross();
    });

    function hamburger_cross() {
        if (isClosed === true) {
            overlay.hide();
            trigger.removeClass('toggled');
            isClosed = false;
        } else {
            overlay.show();
            trigger.addClass('toggled');
            isClosed = true;
        }
    }
    angular.element('.overlay').click(function(e){
        e.stopPropagation();
    });
    angular.element('[data-toggle="offcanvas"]').click(function () {
        trigger.addClass('toggled');
    });
}]);