(function(){
    'use strict';
    angular.module('seHeaderComponent')
        .directive('seApplicationContent',function($timeout){
            return {
                restrict: 'E',
                replace:false,
                transclude:true,
                scope:{},
                template:"<div class=\"appContentHeaderTop\" ng-show=\"userLoggedIn\" ng-transclude=''>",
                controller:'seCustomHeaderController'
            };
        });
})();
