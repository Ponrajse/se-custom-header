(function(){
  'use strict';
  angular.module('seHeaderComponent')
    .directive('seHeaderTopMenu',function($timeout){
      return {
        restrict: 'E',
        transclude :true,
        replace:false,
        scope: {},
        template: "<div id=\"se-header-top-menu\" ng-transclude=''></div>",
        controller:'seCustomHeaderController'
      };
    });
})();
