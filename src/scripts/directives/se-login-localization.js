(function(){
    'use strict';
    angular.module('seHeaderComponent')
        .directive('seLoginLocalization',function($timeout,authenticationService,$q){
            return {
                restrict: 'E',
                transclude :true,
                replace:false,
                scope:{
                  localeVisibility:'=',
                  locale:'=',
                  onbeforeLogout:'&',
                  disableLocale:'='
                },
                templateUrl:'se-login-localization.html',
                controller:'seCustomHeaderController',
                link: function (scope,elem,attrs) {
                    scope.login=function () {
                        authenticationService.getLoginService().login();
                    };
                    scope.logout = function () {
                        if (angular.isDefined(attrs.onbeforeLogout)) {
                            scope.onbeforeLogout()().then(function() {
                                authenticationService.getLoginService().logout();
                            });
                        } else {
                            authenticationService.getLoginService().logout();
                        }
                    };
                }
            };
        });
})();
