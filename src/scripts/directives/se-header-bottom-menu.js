(function(){
  'use strict';
  angular.module('seHeaderComponent')
    .directive('seHeaderBottomMenu',function(){
      return {
        restrict: 'E',
        transclude :true,
        replace:false,
        template: "<div id=\"se-header-bottom-menu\"><div class='appContentHeaderBottom' ng-show='userLoggedIn' ng-transclude=''></div></div>",
        controller:'seCustomHeaderController'
      };
    });
})();
