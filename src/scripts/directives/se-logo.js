(function(){
    'use strict';
    angular.module('seHeaderComponent')
        .directive('seLogo',function($timeout){
            return {
                restrict: 'E',
                replace:false,
                transclude:true,
                scope:{
                    logoSrc:'@',
                    logoLink:'@'
                },
                template:"<div class=\"se-logo\"><a href={{logoLink}} ><img alt=\"schneider logo\" src={{logoSrc}}></a></div>",
                controller:'seCustomHeaderController'
            };
        });
})();
