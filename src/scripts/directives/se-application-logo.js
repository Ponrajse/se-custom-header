(function(){
    'use strict';
    angular.module('seHeaderComponent')
        .directive('seApplicationLogo',function($timeout){
            return {
                restrict: 'E',
                replace:false,
                transclude:true,
                scope:{
                    imgSrc:'@',
                    imgLink:'@'
                },
                template:"<div class=\"appLogo\"><a href={{imgLink}} ><img alt=\"application logo\" src={{imgSrc}}></a></div>",
                controller:'seCustomHeaderController',
              };
        });
})();
