(function(){
  'use strict';
  angular.module('seHeaderComponent')
    .directive('seCustomHeader',function(){
      return {
        restrict: 'E',
        transclude :true,
        replace:false,
        scope: {},
        template: "<div id=\"se-header-template\" ng-transclude=''></div>",
        controller:'seCustomHeaderController'

      };
    });
})();
