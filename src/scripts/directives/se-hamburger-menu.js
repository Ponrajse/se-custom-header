(function(){
    'use strict';
    angular.module('seHeaderComponent')
        .directive('seHamburgerMenu',function($mdMedia){
            return {
                restrict: 'E',
                transclude :true,
                replace:false,
                scope:{
                    visibility:'=',
                },
                templateUrl:'se-hamburger-menu.html',
                controller:'seCustomHeaderController',
                link :function (scope,element,attrs) {
                    angular.element(window).resize(function () {
                        setAppContentWidth();
                    });
                    angular.element(window).load(function () {
                        setAppContentWidth();
                    });
                    function setAppContentWidth() {
                        if(angular.isDefined(attrs.visibility) && attrs.visibility === "false"){
                            if(window.matchMedia('(min-width: 1024px)').matches) {
                                angular.element('.appContentHeaderTop').css('width', 'calc(100% - 327px' + ')');
                            } else {
                                angular.element('.appContentHeaderTop').css('width', 'calc(100% - 303px' + ')');
                            }
                        }
                    }
                }
            };
        });
})();
