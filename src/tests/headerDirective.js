describe('HeaderDirective', function() {
    var authenticationService,mockService, mockServiceConstructor;
    beforeEach(function() {
        module('gettext');
        module('seHeaderComponent');
        module('ngMockE2E');
        module('ngSanitize');
        module('seWebUI');
        module(function($provide) {
            $provide.value('authenticationService', authenticationService);
            $provide.value('mockService', mockService);
        });

    });
    beforeEach(inject(function($rootScope, $compile, $q,_$timeout_,_$controller_) {
        compile = $compile;
        rootScope = $rootScope;
        $q = $q;
        scope = $rootScope.$new();
        $timeout = _$timeout_;
        $controller = _$controller_;
        seHeaderComponent = $controller('seCustomHeaderController',{$scope:scope});
        authenticationService = {
            getLoginService: function() {
                return mockService;
            }
        };
        mockServiceConstructor = function() {
            this.login = function() {
                console.log("Mock login method called!");
            };
            this.getUserProfile = function() {
                var deferred = $q.defer();
                $rootScope.$broadcast('event:loginConfirmed', user);
                deferred.resolve(user);
                return deferred.promise;
            };
            this.logout = function() {
                console.log("Mock logout method called!");
            };
        };
        mockService = new mockServiceConstructor();
        scope.onbeforeLogout = function() {
            var deferred = $q.defer();
            var confirmation = true;
            deferred.resolve(confirmation);
            return deferred.promise;
        };


    }));

    it('should create header template div & Transclude div', function() {
        var htmlContent = '<se-custom-header></se-custom-header>';
        var component = angular.element(htmlContent);
        seCustomHeader = compile(component)(scope);
        scope.$digest();
        //dom elements
        seCustomHeaderTranscludeDiv = angular.element(seCustomHeader[0].querySelector('[ng-transclude=""]'));
        //test
        setemplate = seCustomHeader.find('#se-header-template');
        expect(setemplate.length).toBe(1);
        expect(seCustomHeaderTranscludeDiv.length).toBe(1);
        //scope methods
        scope.$apply();
});

    it('should create header top menu div & Transclude div', function() {
        var htmlContent = '<se-header-top-menu></se-header-top-menu>';
        var component = angular.element(htmlContent);
        seCustomHeaderTop = compile(component)(scope);
        scope.$digest();
        //append to body
        var testMainContainerDiv1 = document.createElement('div');
        var testMainContainerDiv2 = document.createElement('div');
        var testMainContainerDiv3 = document.createElement('div');
        var testMainContainerDiv4 = document.createElement('div');
        var testMainContainerDiv5 = document.createElement('div');
        testMainContainer1 = angular.element(testMainContainerDiv1);
        testMainContainer2 = angular.element(testMainContainerDiv2);
        testMainContainer3 = angular.element(testMainContainerDiv3);
        testMainContainer4 = angular.element(testMainContainerDiv4);
        testMainContainer5 = angular.element(testMainContainerDiv5);

        var testclass1 = testMainContainer1.addClass('hamburgerIcon');
        var testclass2 = testMainContainer2.addClass('appLogo');
        var testclass3 = testMainContainer3.addClass('appContentHeaderTop');
        var testclass4 = testMainContainer4.addClass('loggedInUserInfo');
        var testclass5 = testMainContainer5.addClass('se-logo');

        seCustomHeaderTopTranscludeDiv = angular.element(seCustomHeaderTop[0].querySelector('[ng-transclude=""]'));
        seCustomHeaderTopTranscludeDiv.append(testclass1);
        seCustomHeaderTopTranscludeDiv.append(testclass2);
        seCustomHeaderTopTranscludeDiv.append(testclass3);
        seCustomHeaderTopTranscludeDiv.append(testclass4);
        seCustomHeaderTopTranscludeDiv.append(testclass5);
        seCustomHeaderTopContainer = seCustomHeaderTop.find('#se-header-top-menu');
        topmenudiv1 = angular.element(seCustomHeaderTop).find('.hamburgerIcon');
        topmenudiv2 = angular.element(seCustomHeaderTop).find('.appLogo');
        topmenudiv3 = angular.element(seCustomHeaderTop).find('.appContentHeaderTop');
        topmenudiv4 = angular.element(seCustomHeaderTop).find('.loggedInUserInfo');
        topmenudiv5 = angular.element(seCustomHeaderTop).find('.se-logo');
        //test
        expect(seCustomHeaderTopContainer.length).toBe(1);
        expect(topmenudiv1.length).toBe(1);
        expect(topmenudiv2.length).toBe(1);
        expect(topmenudiv3.length).toBe(1);
        expect(topmenudiv4.length).toBe(1);
        expect(topmenudiv5.length).toBe(1);
        //scope methods
        scope.$apply();
    });

    it('should create header bottom menu div & Transclude div', function() {
        var htmlContent = '<se-header-bottom-menu></se-header-bottom-menu>';
        var component = angular.element(htmlContent);
        seCustomHeaderBottom = compile(component)(scope);
        scope.$digest();
        var testMainContainerDiv1 = document.createElement('div');
        var testclass1 = testMainContainer1.addClass('appContentHeaderBottom');
        //append to body
        seCustomHeaderTranscludeDiv.append(seCustomHeaderBottom); //dom elements
        seCustomHeaderBottomTranscludeDiv = angular.element(seCustomHeaderBottom[0].querySelector('[ng-transclude=""]'));
        seCustomHeaderBottomTranscludeDiv.append(testclass1);
        seCustomHeaderBottomContainer = seCustomHeaderBottom.find('#se-header-bottom-menu');
        bottommenudiv1 = angular.element(seCustomHeaderBottomTranscludeDiv).find('.appContentHeaderBottom');
        //test
        expect(seCustomHeaderBottomContainer.length).toBe(1);
        expect(bottommenudiv1.length).toBe(1);
        //scope methods
        scope.$apply();
    });

    xit('should create hamburger menu & transclude div', function() {
        var htmlContent = '<se-hamburger-menu visibility="true"></se-hamburger-menu>';
        var component = angular.element(htmlContent);
        seHamburgerMenu = compile(component)(scope);
        scope.$digest();
        //append to body
        seCustomHeaderTop.append(seHamburgerMenu); //dom elements
        var seHamburgerMenuScope = seHamburgerMenu.isolateScope();
        //scope
        seHamburgerMenuDiv = angular.element(seHamburgerMenu[0].querySelector('[ng-transclude=""]'));
        seHamburgerMenuDiv.append("test");
        sideDrawerContainer = seHamburgerMenu.find('#sidebar-wrapper');
        //test
        expect(sideDrawerContainer.length).toBe(1);
        expect(seHamburgerMenuDiv[0].innerHTML).toBe("test");
        expect(seHamburgerMenuScope.visibility).toBe(true);
        //scope methods
        scope.$apply();
    });

    it('should create application logo', function() {
        var htmlContent = '<se-application-logo img-src="images/ecoreal.png" img-link="www.schneider-electric.co.in/en"></se-application-logo>';
        var component = angular.element(htmlContent);
        seApplicationLogo = compile(component)(scope);
        scope.$digest();
        //append to body
        seCustomHeaderTop.append(seApplicationLogo); //dom elements
        var seApplicationLogoScope = seApplicationLogo.isolateScope();
        //scope
        applicationLogo = seApplicationLogo.find('.appLogo');
        //test
        expect(applicationLogo.length).toBe(1);
        expect(applicationLogo[0].innerHTML).toBe('<a href="www.schneider-electric.co.in/en"><img alt="application logo" src="images/ecoreal.png"></a>');
        //scope methods
        scope.$apply();
    });
    it('should create application content & transclude div', function() {
        var htmlContent = '<se-application-content></se-application-content>';
        var component = angular.element(htmlContent);
        seApplicationLogo = compile(component)(scope);
        scope.$digest();
        //append to body
        seCustomHeaderTop.append(seApplicationLogo); //dom elements
        var seApplicationLogoScope = seApplicationLogo.isolateScope();
        //scope
        seApplicationLogoDiv = angular.element(seApplicationLogo[0].querySelector('[ng-transclude=""]'));
        seApplicationLogoTop = seApplicationLogo.find('.appContentHeaderTop');
        seApplicationLogoDiv.append("test");
        //test
        expect(seApplicationLogoTop.length).toBe(1);
        expect(seApplicationLogoDiv[0].innerHTML).toBe("test");
        //scope methods
        scope.$apply();
    });

    it('should create schneider logo', function() {
        var htmlContent = '<se-logo logo-src="images/logo-se.svg" logo-link="www.schneider-electric.co.in/en"></se-logo>';
        var component = angular.element(htmlContent);
        seLogo = compile(component)(scope);
        scope.$digest();
        //append to body
        seCustomHeaderTop.append(seLogo); //dom elements
        var seLogoScope = seLogo.isolateScope();
        //scope
        schneiderLogo = seLogo.find('.se-logo');
        //test
        expect(schneiderLogo.length).toBe(1);
        expect(schneiderLogo[0].innerHTML).toBe('<a href="www.schneider-electric.co.in/en"><img alt="schneider logo" src="images/logo-se.svg"></a>');
        //scope methods
        scope.$apply();
    });

    it('should create userprofile,localization and transclude div ', function() {
        var htmlContent = '<se-login-localization locale-visibility="true" locale="locales" onbefore-logout="onbeforeLogout" disable-locale="false" ></se-login-localization>';
        var component = angular.element(htmlContent);
        seUserProfileLocalization = compile(component)(scope);
        scope.$digest();
        //append to body
        seCustomHeaderTop.append(seUserProfileLocalization); //dom elements
        var seUserScope = seUserProfileLocalization.isolateScope();
        //scope
        seUserprofile = angular.element(seUserProfileLocalization[0].querySelector('[ng-transclude=""]'));
        seUserprofile.append("test");
        userProfile = seUserProfileLocalization.find('.userInfo');
        userProfileDropdown = seUserProfileLocalization.find('.dropdown');
        userProfileDropdownMenu = seUserProfileLocalization.find('.dropdown-menu');
        userProfileDropdownSubMenu = seUserProfileLocalization.find('.dropdown-submenu');
        var seLoginListLogin = seUserProfileLocalization.find('li.login-before');
        var seLoginListLoginLink = angular.element(seLoginListLogin).find('a');
        var seLoginListLogout = seUserProfileLocalization.find('li.logout');
        var seLoginListLogoutLink = angular.element(seLoginListLogout).find('a');
        spyOn(mockService, 'login').andCallThrough();
        spyOn(mockService, 'logout').andCallThrough();
        spyOn(seUserScope, 'onbeforeLogout').andCallThrough();
        seLoginListLoginLink.trigger('click');
        expect(mockService.login).toHaveBeenCalled();
        seLoginListLogoutLink.trigger('click');
        expect(mockService.logout).toHaveBeenCalled();
        expect(seUserScope.onbeforeLogout).toHaveBeenCalled();

        //test
        expect(seUserprofile.length).toBe(1);
        expect(userProfile.length).toBe(1);
        expect(userProfileDropdown.length).toBe(2);
        expect(userProfileDropdownMenu.length).toBe(4);
        expect(userProfileDropdownSubMenu.length).toBe(2);
        expect(seUserprofile[0].innerHTML).toBe("test");
        //scope methods
        scope.$apply();
    });
});;