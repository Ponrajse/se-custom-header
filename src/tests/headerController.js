describe('HeaderController', function() {
    var seHeaderComponent;
    beforeEach(function () {
        module('gettext');
        module('seHeaderComponent');
        module('ngMockE2E');
        module('ngSanitize');
        //module('seWebUI');
    });
    beforeEach(inject(function ($controller,_$rootScope_, _$timeout_,_gettextCatalog_,_$q_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $timeout = _$timeout_;
        gettextCatalog = _gettextCatalog_;
        $q =_$q_;
        seCustomHeaderController = $controller('seCustomHeaderController', {
            $scope: $scope
        });

        var locales = [{
            "region": "America",
            "Countries": [{
                "name": "USA - English",
                "value": "en-INT",
                "default": false
            }]
        }, {
            "region": "Europe",
            "Countries": [{
                "name": "France - français",
                "value": "fr-FR",
                "default": true
            }]
        }];
        $scope.locale = locales;
        var user ={"id":"SESA489435","email":"Ponraj.K@non.schneider-electric.com","firstName":"Ponraj","lastName":"K","creationDate":"2018-01-22","userAttributes":[],"applications":[],"userAccess":[],"socialIdentities":[]};
        $scope.user={};
        $scope.user.loginUser = user;
    }));

    it('should set default language',function () {
        $scope.lang = false;
        $scope.defaultLanguage();
        expect($scope.selectedCountryLangauge).toBe('France - français');
    });
    it('should change country language',function () {
        $scope.CountryLanguageOptionChanged($scope.locale[0].Countries[0],$scope.locale[0].region);
        $scope.selectedCountryLangauge = $scope.locale[0].Countries[0].name;
        expect($scope.selectedCountryLangauge).toBe('USA - English');
    });
    it('should watch the user data after logged in',function () {
        var deferred = $q.defer();
        $rootScope.$broadcast('event:loginConfirmed', $scope.user.loginUser);
        deferred.resolve($scope.user.loginUser);
        return deferred.promise;
        var deferred = $q.defer();
        $rootScope.$on('event:loginConfirmed', function(event, data) {
            $scope.loginUser  = data;
            deferred.resolve($scope.loginUser);
            return deferred.promise;
        });
        expect($scope.loginUser).toBe($scope.user.loginUser);
        expect($scope.userName).toBe('PK');
        expect($scope.userLoggedIn).toBe(true);
    });

    it('should watch locales',function () {
      $scope.locales = $scope.locale;
      $scope.$digest();
      expect($scope.selectedCountryLangauge).toBe('France - français')
    });

});