// Gruntfile.js
// our wrapper function (required by grunt and its plugins)
// all configuration goes inside this function
module.exports = function(grunt) {
    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({
        // get the configuration info from package.json ----------------------------
        // this way we can use things like name and version (pkg.name)
        pkg: grunt.file.readJSON('package.json'),
        // all of our configuration will go here
        clean: {
            build: {
                src: ['dist/', '.tmp/']
            }
        },
        jshint: {
            options: {
                reporter: require('jshint-stylish'), // use jshint-stylish to make our errors look and read good
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                globals: {
                    'jQuery': true,
                    'angular': true,
                    '$': true,
                    'console': true
                },
            },
            // when this task is run, lint the Gruntfile and all js files in src
            build: ['Gruntfile.js', 'scripts/**/*.js']
        },
        ngtemplates: {
            build: {
                cwd: 'templates',
                src: '**.html',
                dest: '.tmp/templateCache.js',
                options: {
                    module: 'seHeaderComponent',
                    htmlmin: {
                        collapseWhitespace: true,
                        collapseBooleanAttributes: true
                    }
                }
            }
        },
        // ng-annotate tries to make the code safe for minification automatically
        // by using the Angular long form for dependency injection.
        ngAnnotate: {
            build: {
                files: [{
                    expand: true,
                    src: ['scripts/*.js', 'scripts/**/*.js'],
                    dest: '.tmp'
                }]
            }
        },
        concat: {
            build: {
                src: ['.tmp/scripts/main.js', '.tmp/scripts/**/*.js', '.tmp/templateCache.js'],
                dest: 'dist/<%= pkg.name %>-all.js',
            },
        },
        // configure uglify to minify js files -------------------------------------
        uglify: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            build: {
                files: {
                    'dist/<%= pkg.name %>-all.min.js': ['dist/<%= pkg.name %>-all.js']
                }
            }
        },
        // compile less stylesheets to css -----------------------------------------
        less: {
            build: {
                files: {
                    'dist/<%= pkg.name %>-all.css': 'less/**/*.less'
                }
            }
        },
        // configure cssmin to minify css files ------------------------------------
        cssmin: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            build: {
                files: {
                    'dist/<%= pkg.name %>-all.min.css': 'dist/<%= pkg.name %>-all.css'
                }
            }
        },
        copy: {
            build: {
                files: [
                    // flattens results to a single level
                    {
                        expand: true,
                        flatten: true,
                        src: ['bower.json'],
                        dest: 'dist/',
                        filter: 'isFile'
                    },
                ],
            },
        },
        watch: {
            js: {
                files: ['scripts/**/*.js'],
                tasks: ['jshint', 'ngAnnotate', 'concat'],
                options: {
                    spawn: false,
                },
            },
            html: {
                files: ['templates/**/*.html'],
                tasks: ['ngtemplates', 'ngAnnotate', 'concat'],
                options: {
                    spawn: false,
                },
            },
            css: {
                files: ['less/**/*.less'],
                tasks: ['less', 'cssmin'],
                options: {
                    spawn: false,
                },
            }
        },
        karma: {
            options: {
                configFile: 'config/karma.conf.js',
                force: true
            },
            unit: {
                singleRun: true
            },
            continuous: {
                singleRun: false,
                autoWatch: true
            }
        },
        compress: {
            source: {
                options: {
                    archive: '<%= pkg.name %>-<%= pkg.version %>.zip'
                },
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['**/*'],
                    dest: ''
                }]
            }
        },
        artifactory: {
          options: {
            url: 'https://artifactory-dces.schneider-electric.com',
            repository: 'bower-local',
            username: 'dces-sdk',
            password: 'Dc3$$dKNpm'
          },
          release: {
            files: [{
                expand: true,
                cwd: 'dist',
                src: ['**/*'],
                dest: ''
            }],
            options: {
              publish: [{
                  archive: '<%= pkg.version %>.tgz',
                  id: 'com.schneiderelectric.dces.sdk:<%= pkg.name %>',
                  version: '<%= pkg.version %>', 
                  path: '.tmp/dist-tgz/'
              }]
            }
          }
        },
        sonarRunner: {
            analysis: {
                options: {
                    debug: true,
                    separator: '\n',
                    sonar: {
                        host: {
                            url: 'http://10.194.157.62:9000'
                        },

                        projectKey: 'se-header-component',
                        projectName: 'se-header-component',
                        projectVersion: '<%= pkg.version %>',
                        sources: ['scripts'].join(','),
                        language: 'js',
                        sourceEncoding: 'UTF-8'
                    }
                }
            }
        },
    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    // we can only load these if they are in our package.json
    // make sure you have run npm install so our app can find these
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-artifactory-artifact');
    grunt.loadNpmTasks('grunt-sonar-runner');

    //load tasks
    grunt.registerTask('test', ['jshint', 'karma:unit']);
    grunt.registerTask('build', ['clean', 'ngtemplates', 'ngAnnotate', 'concat', 'uglify', 'less', 'cssmin', 'copy']);
    grunt.registerTask('package', ['build', 'compress','test','run-sonar-analysis']);
    grunt.registerTask('publish', ['build', 'artifactory:release:publish']);
    grunt.registerTask('default', ['test', 'build']);
    grunt.registerTask('devQA', ['default', 'watch']);
    grunt.registerTask('dev', ['build', 'watch']);
    grunt.registerTask('run-sonar-analysis',[ 'sonarRunner']);
};